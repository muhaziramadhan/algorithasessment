function countUniqueValues(arr){
    const count = [];

    for(let i = 0; i < arr.length; i++){
        if (arr[i] === arr[i-1]){
            continue;
        }
        count.push(arr[i]);
    }
    return count.length;
}
console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13])); 
console.log(countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6])); 
console.log(countUniqueValues([]));